package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    String name, role;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
    }

    List<Member> members = new ArrayList<Member>();

    @Override
    public String getName() { return this.name; }

    @Override
    public String getRole() { return this.role; }

    @Override
    public void addChildMember(Member member) {
        if (role.equals("Master") || members.size() < 3) {
            members.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        members.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return members;
    }
}
