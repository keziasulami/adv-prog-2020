package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        Member child = new OrdinaryMember("Sasuke", "Uchiha");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member child = new OrdinaryMember("Sasuke", "Uchiha");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member satu = new OrdinaryMember("satu", "member");
        member.addChildMember(satu);
        Member dua = new OrdinaryMember("dua", "member");
        member.addChildMember(dua);
        Member tiga = new OrdinaryMember("tiga", "member");
        member.addChildMember(tiga);
        Member empat = new OrdinaryMember("empat", "member");
        member.addChildMember(empat);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("GuildMaster", "Master");
        Member satu = new OrdinaryMember("satu", "member");
        master.addChildMember(satu);
        Member dua = new OrdinaryMember("dua", "member");
        master.addChildMember(dua);
        Member tiga = new OrdinaryMember("tiga", "member");
        master.addChildMember(tiga);
        Member empat = new OrdinaryMember("empat", "member");
        master.addChildMember(empat);
        assertEquals(4, master.getChildMembers().size());
    }
}
