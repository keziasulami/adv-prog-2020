package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {

    Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = Mockito.mock(Weapon.class);
    }

    @Test
    public void testMethodGetWeaponName(){
        Mockito.doCallRealMethod().when(weapon).getName();
        Mockito.when(weapon.getName()).thenReturn("Keris");
        String weaponName = weapon.getName();

        Mockito.verify(weapon, Mockito.times(1)).getName();
        assertEquals("Keris", weaponName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        Mockito.doCallRealMethod().when(weapon).getDescription();
        Mockito.when(weapon.getDescription()).thenReturn("Basic Weapon");
        String weaponDescription = weapon.getDescription();

        Mockito.verify(weapon, Mockito.times(1)).getDescription();
        assertEquals("Basic Weapon", weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        Mockito.doCallRealMethod().when(weapon).getWeaponValue();
        Mockito.when(weapon.getWeaponValue()).thenReturn(1);
        int weaponValue = weapon.getWeaponValue();

        Mockito.verify(weapon, Mockito.times(1)).getWeaponValue();
        assertEquals(1, weaponValue);
    }
}
