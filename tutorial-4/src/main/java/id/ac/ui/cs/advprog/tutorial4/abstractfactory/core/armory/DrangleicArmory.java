package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {

    @Override
    public Armor craftArmor() {
        Armor armor = new MetalArmor();
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        Weapon weapon = new ThousandJacker();
        return weapon;
    }

    @Override
    public Skill learnSkill() {
        Skill skill = new ThousandYearsOfPain();
        return skill;
    }
}
