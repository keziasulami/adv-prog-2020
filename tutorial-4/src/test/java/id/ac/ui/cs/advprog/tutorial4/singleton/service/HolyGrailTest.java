package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    HolyGrail holyGrail;
    @BeforeEach
    public void setUp() throws Exception {
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("Become a better person");
    }

    @Test
    public void testSetWish() {
        holyGrail.makeAWish("Become wise");
        HolyWish holyWish = holyGrail.getHolyWish();
        String wish = holyWish.getWish();
        assertEquals("Become wise", wish);
    }

    @Test
    public void testGetWish() {
        HolyWish holyWish = holyGrail.getHolyWish();
        String wish = holyWish.getWish();
        assertEquals("Become a better person", wish);
    }
}
