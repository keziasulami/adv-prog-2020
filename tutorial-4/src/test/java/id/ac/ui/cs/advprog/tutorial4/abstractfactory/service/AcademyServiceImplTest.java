package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @Test
    public void testProduceKnight() {
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertThat(academyService.getKnightAcademies().size()).isEqualTo(2);
        academyService.produceKnight("Drangleic", "majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallGetKnightAcademies() {
        academyService.getKnightAcademies();
        verify(academyRepository, times(1)).getKnightAcademies();
    }

    @Test
    public void testGetKnight() {
        academyService = new AcademyServiceImpl(new AcademyRepository());
        academyService.produceKnight("Drangleic", "majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }
}
