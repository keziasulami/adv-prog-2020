package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import org.springframework.boot.autoconfigure.web.ResourceProperties;

import java.util.List;

public class ChainSpell implements Spell {

    private List<Spell> spells;

    public ChainSpell(List spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spells.size() - 1; i >= 0; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
